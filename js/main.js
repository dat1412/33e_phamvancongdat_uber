
const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";

function tinhGiaTienKmDauTien(car) {
    if (car == UBER_CAR) {
        return 8000;
    }
    if (car == UBER_SUV) {
        return 9000;
    }
    if (car == UBER_BLACK) {
        return 10000;
    }
}

function tinhGiaTienKm1_19(car) {
    switch (car) {
        case UBER_CAR: {
            return 7500;
        } 

        case UBER_SUV: {
            return 8500;
        }
        
        case UBER_BLACK: {
            return 9500;
        }
        
        default:
            return 0;
    }
}

function tinhGiaTienKm19TroDi(car) {
    switch (car) {
        case UBER_CAR: {
            return 7000;
        } 

        case UBER_SUV: {
            return 8000;
        }
        
        case UBER_BLACK: {
            return 9000;
        }
        
        default:
            return 0;
    }
}


// main function
function tinhTienUber() {
    
    var carOption = document.querySelector('input[name="selector"]:checked').value;
    // console.log('carOption: ', carOption);

    var giaTienKmDauTien = tinhGiaTienKmDauTien(carOption);
    // console.log('giaTienKmDauTien: ', giaTienKmDauTien);

    var giaTienKM1_19 = tinhGiaTienKm1_19(carOption);
    // console.log('giaTienKM1_19: ', giaTienKM1_19);

    var giaTienKm19TroDi = tinhGiaTienKm19TroDi(carOption);
    // console.log('giaTiemKm19TroDi: ', giaTiemKm19TroDi);

    document.getElementById("divThanhTien").style.display = "block";

    var soKm = document.getElementById("txt-km").value * 1;

    var result;

    if (soKm <= 1) {
        result = giaTienKmDauTien * soKm;
    } else if (soKm <= 19) { 
        result = (soKm - 1) * giaTienKM1_19 + giaTienKmDauTien;
    } else {
        result = giaTienKmDauTien + 18 * giaTienKM1_19 + (soKm - 19) * giaTienKm19TroDi;
    }

    document.getElementById("xuatTien").textContent = `${result} VND`;

    

    
}

// tong thời gian : 3